async function leerJSON(url) {

  try {
    let response = await fetch(url);
    let user = await response.json();
    return user;
  } catch(err) {
    
    alert(err);
  }
}

//Arreglo con los departamentos no repetidos
function obtenerDepartamentos(datos)
{
	var obtenerDepartamento = new Array();

	for (var i = 0; i < datos.length; i++) {
		obtenerDepartamento[i] = datos[i].departamento_nom; 
	}
	console.log(obtenerDepartamento);
	
	var uniqs = obtenerDepartamento.filter(function(item, index, array) {
  	return array.indexOf(item) === index;
})  //se hace lo de no repetirlos

	console.log(uniqs);
	return uniqs;
}

//menu para mostrar la opciones con el select
function menuDesplegable(datos){
	let arregloDptos = obtenerDepartamentos(datos);
	console.log(arregloDptos.length);
	let menu="";
    for (var i=0; i <= arregloDptos.length; i++) {
    menu += `<option value="${arregloDptos[i]}">${arregloDptos[i]}</option>`;
  	document.getElementById("listaDepartamentos").innerHTML = menu;
	}
}

function prueba(){
	var url="https://raw.githubusercontent.com/yurleyespinel/JSON/main/Covid-19.json";
	leerJSON(url).then(datos=>{
	
	jsFunction(datos);
	})
}

//se hace la tabla de los departamentos
function jsFunction(datos){
	var mensaje = "";
    let casosHombre=0;
	let casosMujer=0;
	//crea tablas	
	var data = new google.visualization.DataTable();
	var data2 = new google.visualization.DataTable();

	var resultado=document.getElementById("r");

	//Encabezados de las tablas:
	crearEncabezados(data);
	crearEncabezadosTorta(data2);

	//Crear las filas:
	data.addRows(2);
	data2.addRows(2);

    let myselect = document.querySelector("#listaDepartamentos");
    let dpto = myselect.options[myselect.selectedIndex].value;
    console.log(dpto);


    for (var i = 0; i < datos.length; i++) {
    	if (dpto == datos[i].departamento_nom) 
    {
    	let sexo=datos[i].sexo;
    	if (sexo == 'F') {
				casosMujer++;
			}
			else{
				casosHombre++;
			}
    	}
    }
    mensaje+=`<h1>Casos de COVID-19: ${dpto}</h1>`
    resultado.innerHTML=mensaje;

    data.setCell(0,0, 'Femenino'); //fila 0, columna 0 (sexo)
	data.setCell(1,0, 'Masculino'); //fila 1, columna 0 (sexo)
	data.setCell(0,1, casosMujer); //fila 0, columna 1 (casos mujer)
	data.setCell(1,1, casosHombre); //fila 1, columna 2 (casos hombre)
    console.log(casosMujer);
    console.log(casosHombre);

    var table = new google.visualization.Table(document.getElementById('table_div'));
	table.draw(data, {showRowNumber: true, width: '50%', height: '100%'});

	crearGrafico(data2,casosHombre, casosMujer);
}


  function crearEncabezados(data)
{
	data.addColumn('string', 'Sexo'); //0
	data.addColumn('number', 'Casos positivos');//1
	//data.addColumn('number', 'Número de casos'); //2
}


function crearGrafico(data2, casosHombre, casosMujer)
{
	//Crear tabla para graficar:
		data2.setCell(0,0,"Casos hombre");
		data2.setCell(0,1,casosHombre);
		data2.setCell(1,0,"Casos mujer");
		data2.setCell(1,1,casosMujer);

		var options = {
          title: 'Casos positivos:',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data2, options);
}

function crearEncabezadosTorta(data2) //En los gráficos la primera columna debe ser string porque va a ser 
								  //la etiqueta que va a tener el gráfico como tal 
{
	data2.addColumn('string', 'Descripción'); //0
	data2.addColumn('number', 'Valor'); //1

} 

//REQUERIMIENTO FUNCIONAL 1
function leerCasos()
{   
	var mensaje = "";
	var url="https://raw.githubusercontent.com/yurleyespinel/JSON/main/Covid-19.json";
	var resultado=document.getElementById("r");
    // esta leyendo los datos
	leerJSON(url).then(datos=>{
	//acá se llamaría el vector general
	leerCasosDepartamento(datos);
	obtenerDepartamentos(datos);
	menuDesplegable(datos);
	//mostrarCasosDepartamentos(datos);

    mensaje+=`<h1>CASOS DE COVID-19: ${datos[0].departamento_nom}</h1>`

    resultado.innerHTML=mensaje;
	})
}

//REQUERIMIENTO FUNCIONAL 2
function leerCasos2()
{
	var mensaje2 = "";
	var url="https://raw.githubusercontent.com/yurleyespinel/JSON/main/Covid-19.json";
	var resultado2=document.getElementById("s");

	leerJSON(url).then(datos=>{
		//LLamar el vector de notas
		leerCasosMunicipio(datos);
		obtenerMunicipios(datos);
		menuDesplegable2(datos);

		mensaje2+=`<h1>Casos de COVID-19: ${datos[0].departamento_nom}</h1>`

    	resultado2.innerHTML=mensaje2;
	})
}

//REQUERIMIENTO FUNCIONAL 3
function leerCasos3()
{
	var url="https://raw.githubusercontent.com/yurleyespinel/JSON/main/Covid-19.json";
	var resultado3=document.getElementById("t");

	leerJSON(url).then(datos=>{
		//LLamar el vector de notas
		leerTodos(datos);
		obtenerDepartamentos3(datos);
		obtenerTotalCasosDpto(datos);
		//obtenerArregloDep(datos);
	})
}


//lo q se repite
function leerCasosDepartamento(datos)
{
	var data = new google.visualization.DataTable();
	var data2 = new google.visualization.DataTable();
	//Encabezados de las tablas:
	crearEncabezados(data);
	crearEncabezadosTorta(data2);
	//Crear las filas:
	data.addRows(2);
	data2.addRows(2);

	let casosHombre=0;
	let casosMujer=0;

	for(let i=0;i<datos.length;i++)//aqui estoy sacando todos los datos de los casos de covid
	{
		let departamento=datos[i].departamento;

		if (departamento == '11') //si el departamento es Bogotá
		{
			let sexo=datos[i].sexo;//obtenemos el sexo
			//console.log(sexo);
			if (sexo == 'F') {
				casosMujer++;
			}
			else{
				casosHombre++;
			}	
		}
		
	}
    //Filas y columnas setCell(indicefila, indiceColumna, dato)
	data.setCell(0,0, 'Femenino'); //fila 0, columna 0 (sexo)
	data.setCell(1,0, 'Masculino'); //fila 1, columna 0 (sexo)
	data.setCell(0,1, casosMujer); //fila 0, columna 1 (casos mujer)
	data.setCell(1,1, casosHombre); //fila 1, columna 2 (casos hombre)
	
	//muestra la tabla en el navegador
	var table = new google.visualization.Table(document.getElementById('table_div'));
	table.draw(data, {showRowNumber: true, width: '50%', height: '100%'});

	crearGrafico(data2,casosHombre, casosMujer);
}



//AQUI EMPIEZA LO DE MUNICIPIOS

//Arreglo con los municipios
function obtenerMunicipios(datos)
{
	var obtenerMunicipio  = new Array();
	for (var i = 0; i < datos.length; i++) {
		obtenerMunicipio[i] = datos[i].ciudad_municipio_nom; 
	}
	console.log(obtenerMunicipio);
	
	var uniqs2 = obtenerMunicipio.filter(function(item, index, array) {
  	return array.indexOf(item) === index;
})

	console.log(uniqs2);
	return uniqs2;
}

function menuDesplegable2(datos){
	let arregloMuni = obtenerMunicipios(datos);
	console.log(arregloMuni.length);
	let menu2="";
    for (var i=0; i <= arregloMuni.length; i++) {
    menu2 += `<option value="${arregloMuni[i]}">${arregloMuni[i]}</option>`;
  	document.getElementById("listaMunicipios").innerHTML = menu2;
	}
}

function prueba2(){
	var url="https://raw.githubusercontent.com/yurleyespinel/JSON/main/Covid-19.json";
	leerJSON(url).then(datos=>{
	
	jsFunction2(datos);
	})
}

function jsFunction2(datos){
	var data = new google.visualization.DataTable();
	var data3 = new google.visualization.DataTable();
	var mensaje2 = "";
	var resultado2=document.getElementById("s");
	//Encabezados de las tablas:
	crearEncabezados2(data);
	crearEncabezadosMTorta(data3);
	//Crear las filas:
	data.addRows(2);
	data3.addRows(2);
    let myselect2 = document.querySelector("#listaMunicipios");
    let muni = myselect2.options[myselect2.selectedIndex].value;
    console.log(muni);
    let relacionados=0;
	let importados=0;
    for (var i = 0; i < datos.length; i++) {
    	if (muni == datos[i].ciudad_municipio_nom) 
    {
    	let fuente=datos[i].fuente_tipo_contagio;
    	if (fuente == 'Relacionado') {
				relacionados++;
			}
			else{
				importados++;
			}
    	}
    }
    mensaje2+=`<h1>Casos de COVID-19: ${muni}</h1>`
    resultado2.innerHTML=mensaje2;

    data.setCell(0,0, 'Relacionados'); //fila 0, columna 0 (sexo)
	data.setCell(1,0, 'Importados'); //fila 1, columna 0 (sexo)
	data.setCell(0,1, relacionados); //fila 0, columna 1 (casos mujer)
	data.setCell(1,1, importados); //fila 1, columna 2 (casos hombre)

    console.log(relacionados);
    console.log(importados);

    var table = new google.visualization.Table(document.getElementById('tabla'));
	table.draw(data, {showRowNumber: true, width: '50%', height: '100%'});

	crearGraficoMTorta(data3, relacionados, importados);
}


//REQUERIMIENTO FUNCIONAL 2
function leerCasosMunicipio(datos)
{
	var data = new google.visualization.DataTable();
	var data3 = new google.visualization.DataTable();
    //Encabezados de las tablas:
	crearEncabezados2(data);
	crearEncabezadosMTorta(data3);
	//Crear las filas:
	data.addRows(2);
	data3.addRows(2);

	let relacionados=0;
	let importados=0;

	for(let i=0;i<datos.length;i++)
	{
		let municipio=datos[i].ciudad_municipio;
		//console.log(municipio);
		if (municipio == '5001') //si el municipio es Medellín
		{
			let fuente=datos[i].fuente_tipo_contagio;
			if (fuente == 'Relacionado') {
				relacionados++;
			}
			else{
				importados++;
			}	
		}
		
	}
	console.log(relacionados);
	console.log(importados);

    //Filas y columnas setCell(indicefila, indiceColumna, dato)
    data.setCell(0,0, 'Relacionados'); //fila 0, columna 0 (sexo)
	data.setCell(1,0, 'Importados'); //fila 1, columna 0 (sexo)
	data.setCell(0,1, relacionados); //fila 0, columna 1 (casos mujer)
	data.setCell(1,1, importados); //fila 1, columna 2 (casos hombre)

    //muestra la tabla en el navegador
	var table = new google.visualization.Table(document.getElementById('tabla'));
	table.draw(data, {showRowNumber: true, width: '50%', height: '100%'});

	crearGraficoMTorta(data3, relacionados, importados);
}


function crearEncabezados2(data) //tabla
{
	data.addColumn('string', 'Fuente'); //0
	data.addColumn('number', 'Número de casos');//1
	//data.addColumn('number', 'Importado'); //2
}

function crearGraficoMTorta(data3, relacionados, importados)
{
	//Crear tabla para graficar:
		data3.setCell(0,0,"Relacionados");
		data3.setCell(0,1,relacionados);
		data3.setCell(1,0,"Importados");
		data3.setCell(1,1,importados);

		var options = {
          title: 'Casos positivos:',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart2_3d'));
        chart.draw(data3, options);
}

function crearEncabezadosMTorta(data3) //En los gráficos la primera columna debe ser string porque va a ser 
								  //la etiqueta que va a tener el gráfico como tal 
{
	data3.addColumn('string', 'Descripción'); //0
	data3.addColumn('number', 'Valor'); //1

}














//REQUERIMIENTO FUNCIONAL 3

//Arreglo con los departamentos
function obtenerDepartamentos3(datos)
{
	var obtenerDepartamento3 = new Array();
	for (var i = 0; i < datos.length; i++) {
		obtenerDepartamento3[i] = datos[i].departamento_nom; 
	}
	console.log(obtenerDepartamento3);
	
	var uniqs3 = obtenerDepartamento3.filter(function(item, index, array) {
  	return array.indexOf(item) === index;
})

	// console.log(uniqs3);
	return uniqs3;
}

function leerTodos(datos)
{   
	//llamando a los vectores
	let arregloDptos = obtenerDepartamentos(datos);
	let arreglosContador = obtenerTotalCasosDpto(datos);
	// console.log(arregloDptos.length);

	//creando las tablas
	var data = new google.visualization.DataTable();
	var data4 = new google.visualization.DataTable();
	
    //creando encabezados
	crearEncabezados3(data);
	crearEncabezadosMTorta2(data4);

	data.addRows(arregloDptos.length);
	data4.addRows(arregloDptos.length);


	for(let i=0;i<arregloDptos.length;i++)
	{
		var contador=0;
		
		data.setCell(i, 0, arregloDptos[i]);
		if (arregloDptos[i] == datos[i].departamento_nom) {
			contador++;
		}
		data.setCell(i, 1, arreglosContador[i]);
		// console.log(contador);
		}

	var table = new google.visualization.Table(document.getElementById('tablaTodos'));
	table.draw(data, {showRowNumber: true, width: '50%', height: '100%'});

	crearGraficoMTorta2(data4,datos);
    }

function obtenerTotalCasosDpto(datos)
{
	let arregloDptos = obtenerDepartamentos(datos);//aqui están los 27 departamentos
	var casosTotalesDep  = new Array();
	var contador=0;
	var aux2=0;
	var aux=0;
	while (aux <= 27){
		contador=0;
		for (var i = 0; i < datos.length; i++) {
		if (datos[i].departamento_nom == arregloDptos[aux]) {
			contador++;
		}	
	}
	
	casosTotalesDep[aux] = contador;
	aux++;
	
	}
	// console.log(casosTotalesDep);
	return casosTotalesDep;
}

function crearEncabezados3(data) //tabla
{
	data.addColumn('string', 'Departamento'); //0
	data.addColumn('number', 'Total casos');//1
	//data.addColumn('number', 'Importado'); //2
}

function crearGraficoMTorta2(data4,datos)
{
	let arregloDptos = obtenerDepartamentos(datos);//aqui están los 27 departamentos
	let casosTotales = obtenerTotalCasosDpto(datos);
	//Crear tabla para graficar:
	for (var i = 0; i < arregloDptos.length; i++) {
		data4.setCell(i,0,arregloDptos[i]);//fila 0, columna 0 (sexo)
		data4.setCell(i,1,casosTotales[i]);//fila 0, columna 0 (sexo)
        
	}

		var options = {
          title: 'Resultado:',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart3_3d'));
        chart.draw(data4, options);
}


function crearEncabezadosMTorta2(data4) //En los gráficos la primera columna debe ser string porque va a ser 
           								  //la etiqueta que va a tener el gráfico como tal 
{
	data4.addColumn('string', 'Nombres'); //0
	data4.addColumn('number', 'Número de casos'); //1

}