![Estadisticas Covid](http://www.madarme.co/portada-web.png) 
# Título del proyecto: Estadísticas Covid-19 🧪

En este proyecto se realiza una weapp que permite:

1. Seleccionar un departamento, mostrar una tabla y una gráfica con sus casos usando como variable de análisis el  sexo de la persona.
2.  Seleccionar un municipio, mostrar en una tabla y una gráfica sus casos dependiendo de la fuente de contagio(Relacionado-Importado).
3. Mostrar los datos por departamento de casos positivos en una tabla y una gráfica.

## Tabla de contenido 📋

1. [Características](#características-)
2. [Contenido del proyecto](#contenido-del-proyecto-)
3. [Tecnologías](#tecnologías-)
4. [IDE](#ide-)
5. [Instalación](#instalación-)
6. [Demo](#demo-)
7. [Autor(es)](#autores-)
8. [Institución Académica](#institución-académica)


## Características 📝

- Proyecto con lectura de datos JSON 
- Carga dinámica del JSON
- Archivo de JSON: [ver](https://www.datos.gov.co/resource/gt2j-8ykr.json)
- Uso de la API de Google charts para hacer las tablas y gráficas de las estadísticas.

## Contenido del proyecto 📄
[index.html](https://gitlab.com/angelycamilacg/covid19/-/blob/master/index.html)

- Archivo principal de invocación a la lectura de JSON.  
- En este archivo el usuario selecciona la opción que quiera consultar.

[rf1.html](https://gitlab.com/angelycamilacg/covid19/-/blob/master/html/ref1.html)

- Archivo donde se selecciona un departamento, de acuerdo a este se muestra una tabla y un gráfico con los casos positivos usando como variable de análisis el  sexo de la persona.

[rf2.html](https://gitlab.com/angelycamilacg/covid19/-/blob/master/html/ref2.html)

- Archivo donde se selecciona un municipio, dependiendo del seleccionado se muestra una tabla y un gráfico con los casos positivos de a cuerdo a la fuente de contagio (relacionados - importados).

[rf3.js](https://gitlab.com/angelycamilacg/covid19/-/blob/master/html/ref23.html)

- Archivo donde se muestra en una tabla y una gráfica los casos totales por departamento.

[estadistica.js](https://gitlab.com/angelycamilacg/covid19/-/blob/master/js/estadistica.js)

- Archivo que contiene la funcionalidad y los métodos implementados para determinar las estadísticas.

## Tecnologías 💻

- HTML5
- JavaScript
- Bootstrap
- CSS3

## IDE 🌐

- El proyecto se desarrolla usando sublime text 3 
- Visor de JSON -(http://jsonviewer.stack.hu/)

## Instalación 🖥
Firefox Developer Edition: [descargar](https://www.mozilla.org/es-ES/firefox/developer/) Software necesario para ver la interacción por consola y depuración del código JS.

- **Descargar** proyecto
- **Invocar** página pizzeria.html desde firefox


## Demo 🌐

Para ver el demo de la aplicación puede dirigirse a: [covid19](http://ufps18.madarme.co/covid/)

## Autor(es) 👩‍👩‍

Proyecto desarrollado por:

- Yurley Gabriela Espinel Santos - yurleygabrielaes@ufps.edu.co
- Angely Camila Calderón García - angelycamilacg@ufps.edu.co

## Institución Académica 

Proyecto desarrollado en la Materia programación web del  [Programa de Ingeniería de Sistemas] de la [Universidad Francisco de Paula Santander]


   [Marco Adarme]: <http://madarme.co>
   [Programa de Ingeniería de Sistemas]:<https://ingsistemas.cloud.ufps.edu.co/>
   [Universidad Francisco de Paula Santander]:<https://ww2.ufps.edu.co/>